﻿using Customer.Service.Models;
using Customer.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace Customer.API.Controllers
{
    public class NDashboardWSController : ApiController
    {
        private ICustomerService service;
        public NDashboardWSController(ICustomerService _service)
        {
            service = _service;
        }

        [Route("NDashboardWS/activeEvents")]
        [HttpGet]
        public HttpResponseMessage Get()
        {
            try
            {
                var result = service.GetActiveEvents();
                var objApiResult = new ApiResult
                {
                    totalCount = result.Count(),
                    pageMetadata = new PageMetaData
                    {
                        Content = result.Cast<object>().ToArray(),
                        count = result.Count()
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, objApiResult);
            }
            catch (Exception exception)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, exception.Message);
            }
        }

        [Route("NDashboardWS/activeEvents/summary/{eventId}")]
        [HttpGet]
        public HttpResponseMessage Summary(int eventId)
        {
            try
            {
                ActiveEventCustomerCall result = service.GetActiveEventById(eventId);
                var objApiResult = new ApiResult
                {
                    totalCount = result != null ? 1 : 0,
                    pageMetadata = new PageMetaData
                    {
                        Content = new object[1] { result },
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, objApiResult);
            }
            catch (Exception exception)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, exception.Message);
            }
        }

        [Route("NDashboardWS/activeEvents/notes/{incidentId}")]
        [HttpGet]
        public HttpResponseMessage Notes(int incidentId)
        {
            try
            {
                var result = service.GetActiveEventsById(incidentId, 0, 0);
                var objApiResult = new ApiResult
                {
                    totalCount = result.Count(),
                    pageMetadata = new PageMetaData
                    {
                        Content = result.Cast<object>().ToArray(),
                        count = result.Count()
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, objApiResult);
            }
            catch (Exception exception)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, exception.Message);
            }
        }

        [Route("NDashboardWS/activeEvents/customercalls/{eventId}/{pageno}/{pagesize}")]
        [HttpGet]
        public HttpResponseMessage CustomerCalls(int eventId, int pageNo, int pageSize)
        {
            try
            {
                var result = service.GetActiveEventsById(eventId, pageNo, pageSize);
                var objApiResult = new ApiResult
                {
                    totalCount = result.Count(),
                    totalPages = result.Count > 0 ? (result.FirstOrDefault().TotalCount / pageSize < pageSize ? 1 : result.FirstOrDefault().TotalCount / pageSize) : 0,
                    pageMetadata = new PageMetaData
                    {
                        Content = result.Cast<object>().ToArray(),
                        count = result.Count(),
                        pageNo = pageNo
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, objApiResult);
            }
            catch (Exception exception)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, exception.Message);
            }
        }

        [Route("NDashboardWS/activeEvents/gettroublecodes/{eventId}")]
        [HttpGet]
        public HttpResponseMessage GetTroubleCodes(int eventId)
        {
            try
            {
                var result = service.GetTroubleCodesByEventId(eventId);
                var objApiResult = new ApiResult
                {
                    totalCount = result.Count(),
                    pageMetadata = new PageMetaData
                    {
                        Content = result.Cast<object>().ToArray(),
                        count = result.Count()
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, objApiResult);
            }
            catch (Exception exception)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, exception.Message);
            }
        }
    }
}
