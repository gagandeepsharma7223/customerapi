﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customer.Service.Models
{
    public class ActiveEventCustomerCall
    {
        public int eventId { get; set; }
        public string custName { get; set; }
        public string custPhone { get; set; }
        public string address { get; set; }
        public DateTimeOffset? callTime { get; set; }
        public string clues { get; set; }
        public string clueDesc { get; set; }
        public string comments { get; set; }
        public int premise { get; set; }
        public string meterId { get; set; }
        public int TotalCount { get; set; }
    }
}
