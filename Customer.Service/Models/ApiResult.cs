﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customer.Service.Models
{
    public class ApiResult
    {
        public int totalCount { get; set; }
        public int totalPages { get; set; }
        public PageMetaData pageMetadata { get; set; }
    }

    public class PageMetaData
    {
        public int pageNo { get; set; }
        public int count { get; set; }
        public object[] Content { get; set; }
    }
}
