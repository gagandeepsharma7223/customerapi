﻿using AutoMapper;
using Customer.Service.Models;

namespace Customer.Service
{
    public class AutoMapperConfig
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile(new CustomerProfile());
            });
        }
    }
    public class CustomerProfile : Profile
    {
        protected override void Configure()
        {
            
        }
    }
}
