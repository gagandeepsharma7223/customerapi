﻿using AutoMapper;
using Customer.Service.Models;
using Customer.Service.Utilities;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;

namespace Customer.Service.Services
{
    public class CustomerService : ICustomerService
    {
        public IEnumerable<ActiveEvent> GetActiveEvents()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            string commandText = "select e.*, crew.crew_name, dmg.assessed, est.description from nms_reporting.active_events e " +
            " left join ( select event_id, array_agg(crew_name) crew_name from nms_reporting.active_events_crew group by event_id) " +
            " crew ON crew.event_id = e.event_id left join (select * from nms_reporting.damage_assessment) dmg on dmg.id = e.dmg_assessment " +
            " left join (select * from nms_reporting.est_source) est on est.source = e.est_source";
            DataTable dt = new DataTable("Table1");

            using (NpgsqlConnection connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();
                NpgsqlDataAdapter da = new NpgsqlDataAdapter(commandText, connection);
                da.Fill(dt);
                return dt.AsEnumerable().Select(x => new ActiveEvent()
                   {
                       status = x["alarm_state"] != DBNull.Value ? x["alarm_state"].ToString() : String.Empty,
                       powerStatus = x["call_type"] != DBNull.Value ? x["call_type"].ToString() : String.Empty,
                       circuit = x["circuit"] != DBNull.Value ? x["circuit"].ToString() : String.Empty,
                       eventId = x["event_id"] != DBNull.Value ? (int)x["event_id"] : 0,
                       numCust = x["num_cust"] != DBNull.Value ? (int)x["num_cust"] : 0,
                       calls = x["calls"] != DBNull.Value ? (int)x["calls"] : 0,
                       eventType = x["event_type"] != DBNull.Value ? (int)x["event_type"] : 0,
                       relationType = x["relation_type"] != DBNull.Value ? (int)x["relation_type"] : 0,
                       dmgAssessment = x["dmg_assessment"] != DBNull.Value ? (int)x["dmg_assessment"] : 0,
                       custCrit = x["cust_crit"] != DBNull.Value ? (int)x["cust_crit"] : 0,
                       custPriority = x["cust_priority"] != DBNull.Value ? (int)x["cust_priority"] : 0,
                       custMed = x["cust_med"] != DBNull.Value ? (int)x["cust_med"] : 0,
                       custKey = x["cust_key"] != DBNull.Value ? (int)x["cust_key"] : 0,
                       wgtCustOut = x["wgt_cust_out"] != DBNull.Value ? (int)x["wgt_cust_out"] : 0,
                       location = x["location"] != DBNull.Value ? x["call_type"].ToString() : String.Empty,
                       device = x["alias"] != DBNull.Value ? x["alias"].ToString() : String.Empty,
                       predictionType = x["prediction_type"] != DBNull.Value ? x["prediction_type"].ToString() : String.Empty,
                       eventTypeDesc = x["event_type_desc"] != DBNull.Value ? x["event_type_desc"].ToString() : String.Empty,
                       division = x["division"] != DBNull.Value ? x["division"].ToString() : String.Empty,
                       estSource = x["est_source"] != DBNull.Value ? x["est_source"].ToString() : String.Empty,
                       etr = x["etr"] != DBNull.Value ? Convert.ToDateTime(x["etr"]) : (DateTime?)null,
                       restoreDate = x["restore_time"] != DBNull.Value ? Convert.ToDateTime(x["restore_time"]) : (DateTime?)null,
                       startDate = x["begin_time"] != DBNull.Value ? Convert.ToDateTime(x["begin_time"]) : (DateTime?)null,
                       clues = x["trouble_code"] != DBNull.Value ? x["trouble_code"].ToString() : String.Empty,
                       dispatchGroup = x["dispatch_group"] != DBNull.Value ? x["dispatch_group"].ToString() : String.Empty,
                       plan = x["plan"] != DBNull.Value ? x["plan"].ToString() : String.Empty,
                       workQueue = x["work_queue"] != DBNull.Value ? x["work_queue"].ToString() : String.Empty,
                       nominalCircuit = x["nominal_circuit"] != DBNull.Value ? x["nominal_circuit"].ToString() : String.Empty,
                       groupType = x["group_type"] != DBNull.Value ? x["group_type"].ToString() : String.Empty,
                       phaseOut = x["phases_out"] != DBNull.Value ? x["phases_out"].ToString() : String.Empty,
                       lastUpdatedBy = x["last_updated_by"] != DBNull.Value ? x["last_updated_by"].ToString() : String.Empty,
                       crewList = x["crew_name"] != DBNull.Value ? x["crew_name"] as string[] : new string[0],
                       description = x["description"] != DBNull.Value ? x["description"].ToString() : String.Empty,
                       substation = x["substation"] != DBNull.Value ? x["substation"].ToString() : String.Empty
                   });
            }
        }

        public List<ActiveEventCustomerCall> GetActiveEventsById(int eventID, int offset, int limit)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            string commandText = "select event_id, name, phone, address || ',' || addr_city as address,input_time, trouble_code, remarks, premise, meter_id from nms_reporting.active_events_customer_calls where event_id = " + eventID + " order by input_time desc ";
            DataTable dt = new DataTable("Table1");
            using (NpgsqlConnection connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();
                NpgsqlDataAdapter da = new NpgsqlDataAdapter(commandText, connection);
                da.Fill(dt);
                var result = dt.AsEnumerable().Select(x => new ActiveEventCustomerCall()
                {
                    custName = x["name"] != DBNull.Value ? x["name"].ToString() : String.Empty,
                    custPhone = x["phone"] != DBNull.Value ? x["phone"].ToString() : String.Empty,
                    address = x["address"] != DBNull.Value ? x["address"].ToString() : String.Empty,
                    eventId = x["event_id"] != DBNull.Value ? (int)x["event_id"] : 0,
                    callTime = x["input_time"] != DBNull.Value ? Convert.ToDateTime(x["input_time"]) : (DateTime?)null,
                    clues = x["trouble_code"] != DBNull.Value ? x["trouble_code"].ToString() : String.Empty,
                    comments = x["remarks"] != DBNull.Value ? x["remarks"].ToString() : String.Empty,
                    meterId = x["meter_id"] != DBNull.Value ? x["meter_id"].ToString() : String.Empty,
                    clueDesc = x["trouble_code"] != DBNull.Value ? EnumHelper<SiteSafetyCodeEnum>.GetEnumDescription(x["trouble_code"].ToString()) : String.Empty
                }).ToList();

                if (limit > 0)
                {
                    if (result != null)
                        result.FirstOrDefault().TotalCount = result.Count;
                    result = result.Skip((offset - 1) * limit).Take(limit).ToList();
                }
                return result;
            }
        }

        public ActiveEventCustomerCall GetActiveEventById(int eventID)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            string commandText = "select event_id, name, phone, address || ',' || addr_city as address,input_time, trouble_code, remarks, premise, meter_id from nms_reporting.active_events_customer_calls where event_id = " + eventID + " order by input_time desc ";
            DataTable dt = new DataTable("Table1");
            using (NpgsqlConnection connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();
                NpgsqlDataAdapter da = new NpgsqlDataAdapter(commandText, connection);
                da.Fill(dt);
                return dt.AsEnumerable().Select(x => new ActiveEventCustomerCall()
                {
                    custName = x["name"] != DBNull.Value ? x["name"].ToString() : String.Empty,
                    custPhone = x["phone"] != DBNull.Value ? x["phone"].ToString() : String.Empty,
                    address = x["address"] != DBNull.Value ? x["address"].ToString() : String.Empty,
                    eventId = x["event_id"] != DBNull.Value ? (int)x["event_id"] : 0,
                    callTime = x["input_time"] != DBNull.Value ? Convert.ToDateTime(x["input_time"]) : (DateTime?)null,
                    clues = x["trouble_code"] != DBNull.Value ? x["trouble_code"].ToString() : String.Empty,
                    comments = x["remarks"] != DBNull.Value ? x["remarks"].ToString() : String.Empty,
                    meterId = x["meter_id"] != DBNull.Value ? x["meter_id"].ToString() : String.Empty,
                    clueDesc = x["trouble_code"] != DBNull.Value ? EnumHelper<SiteSafetyCodeEnum>.GetEnumDescription(x["trouble_code"].ToString()) : String.Empty
                }).FirstOrDefault();
            }
        }

        public IEnumerable<string> GetTroubleCodesByEventId(int eventID)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            string commandText = "select trouble_code from nms_reporting.active_events where event_id= " + eventID;
            DataTable dt = new DataTable("Table1");
            using (NpgsqlConnection connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();
                NpgsqlDataAdapter da = new NpgsqlDataAdapter(commandText, connection);
                da.Fill(dt);
                return dt.AsEnumerable().Select(x => x["trouble_code"] != DBNull.Value ? EnumHelper<SiteSafetyCodeEnum>.GetEnumDescription(x["trouble_code"].ToString()) : String.Empty);
            }
        }
    }
}
